#!/bin/sh
#
# dlntont - Nintendo display-list to nametable map filter
#
# Copyright 2025 Matthew Gilmore
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
# may be used to endorse or promote products derived from this software without
# specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS AS IS AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

BLANK_CHR_ID=36

if test $# -gt 4
then
	printf "Usage: %s [ -b blank_id ] [ displist ]\n" "$0" >&2
	exit 1
fi

HANDLE_BLANK_CHR=0
for i in "$@"
do
	if test $HANDLE_BLANK_CHR -eq 1
	then
		BLANK_CHR_ID=$i
		if test ! -z `printf "%d" $BLANK_CHR_ID | sed 's/[0-9]//g'` || test $BLANK_CHR_ID -gt 255
		then
			printf "Invalid blank character ID: %s\n" "$BLANK_CHR_ID" >&2
			exit 1
		fi

		HANDLE_BLANK_CHR=0
	elif test $i = "-b"
	then
		HANDLE_BLANK_CHR=1
	elif test -f "$i"
	then
		exec <$i
	else
		printf "Unrecognized option: %s\n" $i >&2
		exit 1
	fi
done

BLANK_CHR_ID_OCTAL=`echo "obase=8;$BLANK_CHR_ID" | bc`
FILE_NT0="$TMPDIR"/"nt0.$$.bin"
FILE_NT1="$TMPDIR"/"nt1.$$.bin"
FILE_NT2="$TMPDIR"/"nt2.$$.bin"
FILE_NT3="$TMPDIR"/"nt3.$$.bin"
ADDR_NT0=`echo "ibase=16;2000" | bc`
ADDR_NT1=`echo "ibase=16;2400" | bc`
ADDR_NT2=`echo "ibase=16;2800" | bc`
ADDR_NT3=`echo "ibase=16;2C00" | bc`
ADDR_ATTR_OFF=`echo "ibase=16;3C0" | bc`
ADDR_UB=`echo "ibase=16;3000" | bc`
NT_SIZE=$ADDR_ATTR_OFF

dd if=/dev/zero bs=$NT_SIZE count=1 | tr '\000' "\\$BLANK_CHR_ID_OCTAL" >"$FILE_NT0"
dd if=/dev/zero bs=$NT_SIZE count=1 | tr '\000' "\\$BLANK_CHR_ID_OCTAL" >"$FILE_NT1"
dd if=/dev/zero bs=$NT_SIZE count=1 | tr '\000' "\\$BLANK_CHR_ID_OCTAL" >"$FILE_NT2"
dd if=/dev/zero bs=$NT_SIZE count=1 | tr '\000' "\\$BLANK_CHR_ID_OCTAL" >"$FILE_NT3"

while :
do
	ADDR=`dd bs=1 count=2 | od -v -An -txC | tr '[a-f]' '[A-F]' | sed 's/^[ \t]*\([^ ][^ ]\)[ ]*\([^ ][^ ]\)/ibase=16;\1\2/g' | bc`

	if test -z $ADDR
	then
		break
	fi

	COUNT=`dd bs=1 count=1 | od -v -An -tuC`

	if test $COUNT -ge 128
	then
		ROW_INC=1
	fi
	COUNT=`expr $COUNT % 128`

	if test $COUNT -ge 64
	then
		REPEAT=1
	fi
	COUNT=`expr $COUNT % 64`

	ADDR_OFFSET=0
	if test $ADDR -lt $ADDR_NT0
	then
		OUTFILE=/dev/null
	elif test $ADDR -lt $ADDR_NT1
	then
		ADDR_OFFSET=`expr $ADDR - $ADDR_NT0`
		OUTFILE="$FILE_NT0"
	elif test $ADDR -lt $ADDR_NT2
	then
		ADDR_OFFSET=`expr $ADDR - $ADDR_NT1`
		OUTFILE="$FILE_NT1"
	elif test $ADDR -lt $ADDR_NT3
	then
		ADDR_OFFSET=`expr $ADDR - $ADDR_NT2`
		OUTFILE="$FILE_NT2"
	elif test $ADDR -lt $ADDR_UB
	then
		ADDR_OFFSET=`expr $ADDR - $ADDR_NT3`
		OUTFILE="$FILE_NT3"
	else
		OUTFILE=/dev/null
	fi

	if test $ADDR_OFFSET -ge $ADDR_ATTR_OFF
	then
		OUTFILE=/dev/null
	fi

	if test -z $ROW_INC
	then
		NT_OFFSET=1
	else
		NT_OFFSET=32
	fi

	if test ! -z $REPEAT
	then
		PAYLOAD_BYTE=`dd bs=1 count=1 | od -v -An -toC | sed 's/ //g'`
	fi

	for i in `echo "for(i=0;i<$COUNT;i++)i" | bc`
	do
		if test -z $REPEAT
		then
			PAYLOAD_BYTE=`dd bs=1 count=1 | od -v -An -toC | sed 's/ //g'`
		fi

		if test $ADDR_OFFSET -lt $ADDR_ATTR_OFF
		then
			printf "\\$PAYLOAD_BYTE" | dd of="$OUTFILE" conv=notrunc bs=1 seek=$ADDR_OFFSET
		fi

		ADDR_OFFSET=`expr $ADDR_OFFSET + $NT_OFFSET`
	done
	ROW_INC=""
	REPEAT=""
done

cat "$FILE_NT0" "$FILE_NT1" "$FILE_NT2" "$FILE_NT3"
rm "$FILE_NT0" "$FILE_NT1" "$FILE_NT2" "$FILE_NT3"
