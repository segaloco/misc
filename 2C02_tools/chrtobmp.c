/*
 * chrtobmp - Famicom "character" to bitmap tile converter
 *
 * Copyright 2023 Matthew Gilmore
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

/* from windef.h */

typedef uint8_t		BYTE;
typedef uint16_t	WORD;
typedef int32_t		LONG;
typedef uint32_t	DWORD;
typedef BYTE *		LPBYTE;

/* end from windef.h */

/* from wingdi.h */

typedef struct tagBITMAPFILEHEADER {
	WORD	bfType;
	DWORD	bfSize;
	WORD	bfReserved1;
	WORD	bfReserved2;
	DWORD	bfOffBits;
} BITMAPFILEHEADER, *LPBITMAPFILEHEADER, *PBITMAPFILEHEADER;

#define	BI_RGB	0

typedef struct tagBITMAPINFOHEADER {
	DWORD	biSize;
	LONG	biWidth;
	LONG	biHeight;
	WORD	biPlanes;
	WORD	biBitCount;
	DWORD	biCompression;
	DWORD	biSizeImage;
	LONG	biXPelsPerMeter;
	LONG	biYPelsPerMeter;
	DWORD	biClrUsed;
	DWORD	biClrImportant;
} BITMAPINFOHEADER, *LPBITMAPINFOHEADER, *PBITMAPINFOHEADER;

typedef struct tagRGBQUAD {
	BYTE	rgbBlue;
	BYTE	rgbGreen;
	BYTE	rgbRed;
	BYTE	rgbReserved;
} RGBQUAD;

/* end from wingdi.h */

#define	PALETTE_SIZE	4
#define	TILE_SIZE	16
#define TILE_DIMENSION	8

#define	RGBQUAD_SIZE		4
#define	BITMAPFILEHEADER_SIZE	14
#define	BITMAPINFOHEADER_SIZE	40

#define	HEADER_SIZE	(BITMAPFILEHEADER_SIZE + BITMAPINFOHEADER_SIZE)

typedef struct tile {
	uint8_t raw[TILE_SIZE];
	uint8_t *cooked;
	size_t _cooked_size;
} tile_t;

typedef struct bitmap {
	BITMAPFILEHEADER bmfHeader;
	BITMAPINFOHEADER bmiHeader;
	RGBQUAD *bmiColors;
	LPBYTE data;
	size_t	_bmi_color_bytes;
	size_t	_data_bytes;
} bitmap_t;

typedef struct palette {
	uint8_t red, green, blue;
} palette_t;

palette_t palette_bytes[64] = {
	{ 124, 124, 124 },
	{ 0, 0, 252 },
	{ 0, 0, 188 },
	{ 68, 40, 188 },
	{ 148, 0, 132 },
	{ 168, 0, 32 },
	{ 168, 16, 0 },
	{ 136, 20, 0 },
	{ 80, 48, 0 },
	{ 0, 120, 0 },
	{ 0, 104, 0 },
	{ 0, 88, 0 },
	{ 0, 64, 88 },
	{ 0, 0, 0 },
	{ 0, 0, 0 },
	{ 0, 0, 0 },
	{ 188, 188, 188 },
	{ 0, 120, 248 },
	{ 0, 88, 248 },
	{ 104, 68, 252 },
	{ 216, 0, 204 },
	{ 228, 0, 88 },
	{ 248, 56, 0 },
	{ 228, 92, 16 },
	{ 172, 124, 0 },
	{ 0, 184, 0 },
	{ 0, 168, 0 },
	{ 0, 168, 68 },
	{ 0, 136, 136 },
	{ 0, 0, 0 },
	{ 0, 0, 0 },
	{ 0, 0, 0 },
	{ 248, 248, 248 },
	{ 60, 188, 252 },
	{ 104, 136, 252 },
	{ 152, 120, 248 },
	{ 248, 120, 248 },
	{ 248, 88, 152 },
	{ 248, 120, 88 },
	{ 252, 160, 68 },
	{ 248, 184, 0 },
	{ 184, 248, 24 },
	{ 88, 216, 84 },
	{ 88, 248, 152 },
	{ 0, 232, 216 },
	{ 120, 120, 120 },
	{ 0, 0, 0 },
	{ 0, 0, 0 },
	{ 252, 252, 252 },
	{ 164, 228, 252 },
	{ 184, 184, 248 },
	{ 216, 184, 248 },
	{ 248, 184, 248 },
	{ 248, 164, 192 },
	{ 240, 208, 176 },
	{ 252, 224, 168 },
	{ 248, 216, 120 },
	{ 216, 248, 120 },
	{ 184, 248, 184 },
	{ 184, 248, 216 },
	{ 0, 252, 252 },
	{ 248, 216, 248 },
	{ 0, 0, 0 },
	{ 0, 0, 0 },
};

bitmap_t *bitmap_init(bitmap_t *bitmap, int bpp)
{
	LPBITMAPFILEHEADER bmfHeader = &bitmap->bmfHeader;
	LPBITMAPINFOHEADER bmiHeader = &bitmap->bmiHeader;

	bmfHeader->bfType = 'B' + ('M' << CHAR_BIT);
	bmfHeader->bfSize = bmfHeader->bfOffBits = HEADER_SIZE;

	bmiHeader->biSize = BITMAPINFOHEADER_SIZE;
	bmiHeader->biWidth = TILE_DIMENSION;
	bmiHeader->biHeight = TILE_DIMENSION;
	bmiHeader->biPlanes = 1;
	bmiHeader->biBitCount = bpp;
	bmiHeader->biCompression = BI_RGB;

	bitmap->bmiColors = NULL;
	bitmap->data = NULL;

	return bitmap;
}

RGBQUAD *rgbquad_from_palette(RGBQUAD *rgbquad, uint8_t palette)
{
	rgbquad->rgbBlue	= (BYTE) palette_bytes[palette].blue;
	rgbquad->rgbGreen	= (BYTE) palette_bytes[palette].green;
	rgbquad->rgbRed		= (BYTE) palette_bytes[palette].red;
	rgbquad->rgbReserved	= 0;

	return rgbquad;
}

bitmap_t *bitmap_add_palette(bitmap_t *bitmap, uint8_t *palette, size_t size, int bpp)
{
	int i;

	if (bitmap->bmiColors == NULL) {
		bitmap->_bmi_color_bytes = size * sizeof (*bitmap->bmiColors)*bpp;

		bitmap->bmiColors = malloc(bitmap->_bmi_color_bytes);
		bitmap->bmfHeader.bfSize += bitmap->_bmi_color_bytes;
		bitmap->bmfHeader.bfOffBits = bitmap->bmfHeader.bfSize;
	}

	for (i = 0; i < size; i++)
		rgbquad_from_palette(&bitmap->bmiColors[i], palette[i]);

	return bitmap;
}

bitmap_t *bitmap_add_tile(bitmap_t *bitmap, tile_t *tile)
{
	bitmap->_data_bytes = tile->_cooked_size;
	bitmap->data = malloc(bitmap->_data_bytes);
	memcpy(bitmap->data, tile->cooked, bitmap->_data_bytes);
	bitmap->bmfHeader.bfSize += bitmap->_data_bytes;

	return bitmap;
}

#define	putw_m(word, file)			\
	putc(((word) & UCHAR_MAX), (file));	\
	putc(((word) >> CHAR_BIT), (file))

#define	putd_m(dword, file)					\
	putc(((dword) & UCHAR_MAX), (file));			\
	putc((((dword) >> CHAR_BIT) & UCHAR_MAX), (file));	\
	putc((((dword) >> (CHAR_BIT*2)) & UCHAR_MAX), (file));	\
	putc(((dword) >> (CHAR_BIT*3)), (file))

void bitmap_write(FILE *file, bitmap_t *bitmap)
{
	int i;

	const LPBITMAPFILEHEADER bmfHeader = &bitmap->bmfHeader;
	const LPBITMAPINFOHEADER bmiHeader = &bitmap->bmiHeader;
	const RGBQUAD *bmiColors = bitmap->bmiColors;

	putw_m(bmfHeader->bfType, file);
	putd_m(bmfHeader->bfSize, file);
	putw_m(bmfHeader->bfReserved1, file);
	putw_m(bmfHeader->bfReserved2, file);
	putd_m(bmfHeader->bfOffBits, file);

	putd_m(bmiHeader->biSize, file);
	putd_m(bmiHeader->biWidth, file);
	putd_m(bmiHeader->biHeight, file);
	putw_m(bmiHeader->biPlanes, file);
	putw_m(bmiHeader->biBitCount, file);
	putd_m(bmiHeader->biCompression, file);
	putd_m(bmiHeader->biSizeImage, file);
	putd_m(bmiHeader->biXPelsPerMeter, file);
	putd_m(bmiHeader->biYPelsPerMeter, file);
	putd_m(bmiHeader->biClrUsed, file);
	putd_m(bmiHeader->biClrImportant, file);

	for (i = 0; i < (bitmap->_bmi_color_bytes / sizeof (*bitmap->bmiColors)); i++) {
		putc(bmiColors[i].rgbBlue, file);
		putc(bmiColors[i].rgbGreen, file);
		putc(bmiColors[i].rgbRed, file);
		putc(bmiColors[i].rgbReserved, file);
	}

	for (i = 0; i < bitmap->_data_bytes; i++)
		putc(bitmap->data[i], file);
}

void bitmap_free(bitmap_t *bitmap)
{
	if (bitmap->bmiColors != NULL) {
		bitmap->_bmi_color_bytes = 0;
		free(bitmap->bmiColors);
		bitmap->bmiColors = NULL;
	}

	if (bitmap->data != NULL) {
		bitmap->_data_bytes = 0;
		free(bitmap->data);
		bitmap->data = NULL;
	}
}

void fami_to_4bpp(tile_t *tile)
{
	const int from_bpp = 2;
	int i;
	int j;
	uint8_t *cooked;
	uint8_t raw_high;
	uint8_t raw_low;

	tile->_cooked_size = TILE_SIZE * from_bpp;

	cooked = malloc(tile->_cooked_size);

	for (i = 0; i < (TILE_SIZE / from_bpp); i++) {
		j = (TILE_SIZE / from_bpp) - i;

		raw_low = tile->raw[i];
		raw_high = tile->raw[i+8];

		cooked[4*j-4] = ((((raw_high >> 6) & 0x2)|((raw_low >> 7) & 0x1)) << 4) | (((raw_high >> 5) & 0x2)|((raw_low >> 6) & 0x1));
		cooked[4*j-3] = ((((raw_high >> 4) & 0x2)|((raw_low >> 5) & 0x1)) << 4) | (((raw_high >> 3) & 0x2)|((raw_low >> 4) & 0x1));
		cooked[4*j-2] = ((((raw_high >> 2) & 0x2)|((raw_low >> 3) & 0x1)) << 4) | (((raw_high >> 1) & 0x2)|((raw_low >> 2) & 0x1));
		cooked[4*j-1] = ((((raw_high >> 0) & 0x2)|((raw_low >> 1) & 0x1)) << 4) | (((raw_high << 1) & 0x2)|((raw_low >> 0) & 0x1));
	}

	tile->cooked = cooked;
}

void tile_free(tile_t *tile)
{
	if (tile->cooked != NULL) {
		tile->_cooked_size = 0;
		free(tile->cooked);
		tile->cooked = NULL;
	}
}

int main(int argc, char *argv[])
{
	const int bpp = 4;
	const size_t palette_size = PALETTE_SIZE;
	const size_t tile_size = 16 * 2;
	bitmap_t bitmap = { 0 };

	tile_t tile;
	uint8_t palette[PALETTE_SIZE];

	if (argc > 2) {
		fprintf(stderr, "Usage: %s [ palette ]\n", argv[0]);
		return EXIT_FAILURE;
	} else if (argc == 2) {
		FILE *palette_file;

		palette_file = fopen(argv[1], "rb");
		fread(palette, 1, palette_size, palette_file);
		fclose(palette_file);
	} else {
		palette[0] = 0xE;
		palette[1] = 0x29;
		palette[2] = 0x37;
		palette[3] = 0x11;
	}

	fread(tile.raw, 1, TILE_SIZE, stdin);
	fami_to_4bpp(&tile);

	bitmap_init(&bitmap, bpp);
	bitmap_add_palette(&bitmap, palette, palette_size, bpp);
	bitmap_add_tile(&bitmap, &tile);
	tile_free(&tile);

	bitmap_write(stdout, &bitmap);
	bitmap_free(&bitmap);

	return EXIT_SUCCESS;
}
