/*
 * chrtopng - Famicom "character" to PNG tile converter
 *
 * Copyright 2025 Matthew Gilmore
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include <png.h>

#define	PPU_CHR_SIZE_BYTES	16
#define	PPU_CHR_WIDTH_PX	8
#define	PPU_CHR_HEIGHT_PX	8
#define	PPU_CHR_DEPTH_PX	2
#define	PPU_PAL_BYTES		(4)

#define	PPU_BANK_CHR_MAX	256

static int row_tile_max = 16;
static int tile_max = PPU_BANK_CHR_MAX;

static void png_init(png_structp *png_write_obj, png_infop *png_info, size_t size, char *pal);
static void png_finish(png_structp *png_write_obj, png_infop *png_info);
static size_t calc_width(size_t size);
static size_t calc_height(size_t size);

int main(int argc, char *argv[])
{
	FILE *pal_file = NULL;
	int i, j;
	int chr_idx;
	size_t size;
	size_t width;
	size_t height;
	char *chr;
	char pal[4];
	char pixels[4];
	png_structp png_write_obj;
	png_infop png_info;
	png_bytep row;

	if (argc > 8) {
		fprintf(stderr, "Usage: %s [ -r row_tile_max ] [ -p palette ] [ -m tile_max ] [ file ]\n", argv[0]);
		return EXIT_FAILURE;
	}

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case 'r':
				if (sscanf(argv[++i], "%d", &row_tile_max) == 0) {
					fprintf(stderr, "Unrecognized row tile max: %s\n", argv[i]);
					return EXIT_FAILURE;
				}
				break;
			case 'p':
				if ((pal_file = fopen(argv[++i], "rb")) == NULL) {
					fprintf(stderr, "Cannot open file: %s\n", argv[i]);
					return EXIT_FAILURE;
				}
				break;
			case 'm':
				if (sscanf(argv[++i], "%d", &tile_max) == 0) {
					fprintf(stderr, "Unrecognized tile max: %s\n", argv[i]);
					return EXIT_FAILURE;
				}
				break;
			default:
				fprintf(stderr, "Unrecognized option: -%c\n", argv[i][1]);
				return EXIT_FAILURE;
			}
		} else {
			if (freopen(argv[i], "rb", stdin) == NULL) {
				fprintf(stderr, "Cannot open file: %s\n", argv[i]);
				return EXIT_FAILURE;
			}
		}
	}

	chr = malloc(tile_max*PPU_CHR_SIZE_BYTES);

	size = fread(chr, sizeof (char), tile_max*PPU_CHR_SIZE_BYTES, stdin);

	if (size == 0) {
		fprintf(stderr, "zero bytes read\n");
		return EXIT_FAILURE;
	}

	if (pal_file != NULL) {
		if (fread(pal, sizeof (char), sizeof (pal), pal_file) != sizeof (pal)) {
			fprintf(stderr, "Failed to read palette file.\n");
			return EXIT_FAILURE;
		}
		fclose(pal_file);
	} else {
		pal[0] = 0x0F;
		pal[1] = 0x16;
		pal[2] = 0x27;
		pal[3] = 0x18;
	}

	png_init(&png_write_obj, &png_info, size, pal);

	width = calc_width(size);
	height = calc_height(size);
	row = png_malloc(png_write_obj, (width / (PPU_CHR_WIDTH_PX/PPU_CHR_DEPTH_PX))*sizeof (png_byte));
	for (i = 0; i < height; i++) {
		for (j = 0; j < (width / PPU_CHR_WIDTH_PX); j++) {
			chr_idx = (((i / PPU_CHR_HEIGHT_PX) * (PPU_CHR_SIZE_BYTES * (width / PPU_CHR_WIDTH_PX))) + (i % PPU_CHR_HEIGHT_PX)) + (j * PPU_CHR_SIZE_BYTES);
			
			if (chr_idx < size) {
				pixels[0] = ((chr[chr_idx]&0x80)>>1)|((chr[chr_idx+8]&0x80)>>0);
				pixels[1] = ((chr[chr_idx]&0x40)>>2)|((chr[chr_idx+8]&0x40)>>1);
				pixels[2] = ((chr[chr_idx]&0x20)>>3)|((chr[chr_idx+8]&0x20)>>2);
				pixels[3] = ((chr[chr_idx]&0x10)>>4)|((chr[chr_idx+8]&0x10)>>3);
				row[(2*j)] = pixels[0]|pixels[1]|pixels[2]|pixels[3];

				pixels[0] = ((chr[chr_idx]&0x08)<<3)|((chr[chr_idx+8]&0x08)<<4);
				pixels[1] = ((chr[chr_idx]&0x04)<<2)|((chr[chr_idx+8]&0x04)<<3);
				pixels[2] = ((chr[chr_idx]&0x02)<<1)|((chr[chr_idx+8]&0x02)<<2);
				pixels[3] = ((chr[chr_idx]&0x01)<<0)|((chr[chr_idx+8]&0x01)<<1);
				row[(2*j)+1] = pixels[0]|pixels[1]|pixels[2]|pixels[3];
			} else {
				row[2*j] = 0;
				row[(2*j)+1] = 0;
			}
		}

		png_write_row(png_write_obj, row);
	}
	png_free(png_write_obj, row);

	free(chr);

	png_finish(&png_write_obj, &png_info);
}

static png_colorp palette_init(png_colorp palette, char *pal_bytes);
static png_colorp palette;

static void png_init(png_structp *png_write_obj, png_infop *png_info, size_t size, char *pal)
{
	int width, height;

	if ((*png_write_obj = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL)) == NULL) {
		exit(EXIT_FAILURE);
	}

	if ((*png_info = png_create_info_struct(*png_write_obj)) == NULL) {
		png_destroy_write_struct(png_write_obj, NULL);
		exit(EXIT_FAILURE);
	}

	if (setjmp(png_jmpbuf(*png_write_obj))) {
		png_destroy_write_struct(png_write_obj, png_info);
		exit(EXIT_FAILURE);
	}

	png_init_io(*png_write_obj, stdout);

	width = calc_width(size);
	height = calc_height(size);

	png_set_IHDR(*png_write_obj, *png_info, width, height, PPU_CHR_DEPTH_PX, PNG_COLOR_TYPE_PALETTE, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
	palette = png_malloc(*png_write_obj, PPU_PAL_BYTES*sizeof (png_color));

	palette_init(palette, pal);

	png_set_PLTE(*png_write_obj, *png_info, palette, PPU_PAL_BYTES);

	png_write_info(*png_write_obj, *png_info);
}

static void png_finish(png_structp *png_write_obj, png_infop *png_info)
{
	png_write_end(*png_write_obj, *png_info);
	png_free(*png_write_obj, palette);
	png_destroy_write_struct(png_write_obj, png_info);
}

typedef struct palette {
	char red, green, blue;
} palette_t;

static palette_t palette_tbl[64] = {
	{ 124, 124, 124 },
	{ 0, 0, 252 },
	{ 0, 0, 188 },
	{ 68, 40, 188 },
	{ 148, 0, 132 },
	{ 168, 0, 32 },
	{ 168, 16, 0 },
	{ 136, 20, 0 },
	{ 80, 48, 0 },
	{ 0, 120, 0 },
	{ 0, 104, 0 },
	{ 0, 88, 0 },
	{ 0, 64, 88 },
	{ 0, 0, 0 },
	{ 0, 0, 0 },
	{ 0, 0, 0 },
	{ 188, 188, 188 },
	{ 0, 120, 248 },
	{ 0, 88, 248 },
	{ 104, 68, 252 },
	{ 216, 0, 204 },
	{ 228, 0, 88 },
	{ 248, 56, 0 },
	{ 228, 92, 16 },
	{ 172, 124, 0 },
	{ 0, 184, 0 },
	{ 0, 168, 0 },
	{ 0, 168, 68 },
	{ 0, 136, 136 },
	{ 0, 0, 0 },
	{ 0, 0, 0 },
	{ 0, 0, 0 },
	{ 248, 248, 248 },
	{ 60, 188, 252 },
	{ 104, 136, 252 },
	{ 152, 120, 248 },
	{ 248, 120, 248 },
	{ 248, 88, 152 },
	{ 248, 120, 88 },
	{ 252, 160, 68 },
	{ 248, 184, 0 },
	{ 184, 248, 24 },
	{ 88, 216, 84 },
	{ 88, 248, 152 },
	{ 0, 232, 216 },
	{ 120, 120, 120 },
	{ 0, 0, 0 },
	{ 0, 0, 0 },
	{ 252, 252, 252 },
	{ 164, 228, 252 },
	{ 184, 184, 248 },
	{ 216, 184, 248 },
	{ 248, 184, 248 },
	{ 248, 164, 192 },
	{ 240, 208, 176 },
	{ 252, 224, 168 },
	{ 248, 216, 120 },
	{ 216, 248, 120 },
	{ 184, 248, 184 },
	{ 184, 248, 216 },
	{ 0, 252, 252 },
	{ 248, 216, 248 },
	{ 0, 0, 0 },
	{ 0, 0, 0 },
};

static png_colorp palette_init(png_colorp palette, char *pal_bytes)
{
	int i;

	for (i = 0; i < PPU_PAL_BYTES; i++) {
		palette[i].red = palette_tbl[pal_bytes[i]].red;
		palette[i].green = palette_tbl[pal_bytes[i]].green;
		palette[i].blue = palette_tbl[pal_bytes[i]].blue;
	}

	return palette;
}

static size_t calc_width(size_t size)
{
	if (size >= (row_tile_max*PPU_CHR_SIZE_BYTES)) {
		return PPU_CHR_WIDTH_PX * row_tile_max;
	} else {
		return PPU_CHR_WIDTH_PX * (((size - 1) / PPU_CHR_SIZE_BYTES) + 1);
	}
}

static size_t calc_height(size_t size)
{
	return PPU_CHR_HEIGHT_PX * (((size - 1) / (PPU_CHR_SIZE_BYTES*row_tile_max)) + 1);
}
