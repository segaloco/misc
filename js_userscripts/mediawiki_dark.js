// ==UserScript==
// @name         Wikipedia
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  try to take over the world!
// @author       You
// @match        https://en.wikipedia.org/*
// @grant        none
// ==/UserScript==

(() => {
    'use strict';

    const BG_COLOR = "#121212";
    const FG_COLOR = "#A0A0A0";
    const LINK_BLUE = "#3366cc";
    const BG_TONE = "#202020";
    const BG_HIGHLIGHT = "#333333";
    const BG_NAVBOX_BLUE = "#2A2A2A";
    const INVERT_100 = "invert(100%)";

    const th_elems = document.getElementsByTagName("th");
    const mp_topbanner = document.getElementById("mp-topbanner");
    const mp_left = document.getElementById("mp-left");
    const mp_right = document.getElementById("mp-right");
    const mp_lower = document.getElementById("mp-lower");
    const mp_h2s = document.getElementsByClassName("mp-h2");
    const vector_toc = document.getElementById("vector-toc");
    const vector_header_containers = document.getElementsByClassName("vector-header-container");
    const vector_tab_noicons = document.getElementsByClassName("vector-tab-noicon");
    const mw_page_containers = document.getElementsByClassName("mw-page-container");
    const mw_logo_wordmarks = document.getElementsByClassName("mw-logo-wordmark");
    const mw_logo_taglines = document.getElementsByClassName("mw-logo-tagline");
    const mw_ui_icons = document.getElementsByClassName("mw-ui-icon");
    const mw_ui_buttons = document.getElementsByClassName("mw-ui-button");
    const ns = document.getElementsByClassName("n");
    const ps = document.getElementsByClassName("p");
    const h1s = document.getElementsByTagName("h1");
    const h2s = document.getElementsByTagName("h2");
    const h3s = document.getElementsByTagName("h3");
    const h4s = document.getElementsByTagName("h4");
    const h5s = document.getElementsByTagName("h5");
    const h6s = document.getElementsByTagName("h6");
    const amboxen = document.getElementsByClassName("ambox");
    const sidebars = document.getElementsByClassName("sidebar");
    const infoboxen = document.getElementsByClassName("infobox");
    const thumbinners = document.getElementsByClassName("thumbinner");
    const portalborders = document.getElementsByClassName("portalborder");
    const navboxen = document.getElementsByClassName("navbox");
    const side_boxen = document.getElementsByClassName("side-box");
    const navbox_titles = document.getElementsByClassName("navbox-title");
    const navbox_groups = document.getElementsByClassName("navbox-group");
    const navbox_subgroups = document.getElementsByClassName("navbox-subgroup");
    const navbox_evens = document.getElementsByClassName("navbox-even");
    const quoteboxen = document.getElementsByClassName("quotebox");
    const catlinks = document.getElementById("catlinks");
    const mw_headers = document.getElementsByClassName("mw-header");
    const mw_footers = document.getElementsByClassName("mw-footer");
    const style_elem = document.createElement("style");
    const code_elems = document.getElementsByTagName("code");
    const nvs = document.getElementsByClassName("nv");
    const fallback_images = document.getElementsByClassName("mwe-math-fallback-image-inline");
    const pre_elems = document.getElementsByTagName("pre");
    const wikitables = document.getElementsByClassName("wikitable");
    const hlls = document.getElementsByClassName("hll");
    const quotebox_titles = document.getElementsByClassName("quotebox-title");
    const vector_icons = document.getElementsByClassName("vector-icon");
    const cdx_buttons = document.getElementsByClassName("cdx-button");

    const mwe_popup_observer = new MutationObserver((mutationList, observer) => {
        for (const mutation of mutationList) {
            if (mutation.type === "childList") {
                for (const node of mutation.addedNodes) {
                    if (node.classList.contains("mwe-popups")) {
                        const mwe_popups_containers = node.getElementsByClassName("mwe-popups-container");
                        const mwe_popups_extracts = node.getElementsByClassName("mwe-popups-extract");

                        for (const mwe_popups_container of mwe_popups_containers) {
                            mwe_popups_container.style.backgroundColor = BG_TONE;
                        }

                        for (const mwe_popups_extract of mwe_popups_extracts) {
                            mwe_popups_extract.style.color = FG_COLOR;
                        }
                    }
                }
            }
        }
    });

    style_elem.innerText = ".vector-toc:after { background: transparent !important; } .mwe-popups-extract::after { background-image: linear-gradient(to right, rgba(255, 255, 255, 0), " + BG_TONE + " 100%) !important; }";
    document.head.appendChild(style_elem);

    document.body.style.color = FG_COLOR;
    document.body.style.backgroundColor = BG_COLOR;

    mwe_popup_observer.observe(document.body, { childList: true });

    for (const cdx_button of cdx_buttons) {
        cdx_button.style.backgroundColor = BG_HIGHLIGHT;
    }

    for (const quotebox_title of quotebox_titles) {
        quotebox_title.style.backgroundColor = BG_HIGHLIGHT;
    }

    for (const hll of hlls) {
        hll.style.backgroundColor = BG_HIGHLIGHT;
    }

    for (const wikitable of wikitables) {
        wikitable.style.backgroundColor = BG_COLOR;
        wikitable.style.color = FG_COLOR;
    }

    for (const vector_icon of vector_icons) {
        vector_icon.style.filter = INVERT_100;
    }

    for (const th of th_elems) {
        th.style.backgroundColor = BG_TONE;
    }

    for (const fallback_image of fallback_images) {
        fallback_image.style.filter = INVERT_100;
    }

    for (const pre_elem of pre_elems) {
        pre_elem.style.backgroundColor = BG_COLOR;
        pre_elem.style.color = FG_COLOR;
    }

    for (const n of ns) {
        n.style.color = FG_COLOR;
    }

    for (const p of ps) {
        p.style.color = FG_COLOR;
    }

    for (const nv of nvs) {
        nv.style.color = FG_COLOR;
    }

    for (const code_elem of code_elems) {
        code_elem.style.backgroundColor = BG_COLOR;
        code_elem.style.color = FG_COLOR;
    }

    for (const mw_logo_wordmark of mw_logo_wordmarks) {
        mw_logo_wordmark.style.filter = INVERT_100;
    }

    for (const mw_logo_tagline of mw_logo_taglines) {
        mw_logo_tagline.style.filter = INVERT_100;
    }

    for (const mw_ui_icon of mw_ui_icons) {
        mw_ui_icon.style.filter = INVERT_100;
    }

    for (const mw_ui_button of mw_ui_buttons) {
        mw_ui_button.style.backgroundColor = BG_COLOR;
    }

    for (const side_box of side_boxen) {
        side_box.style.backgroundColor = BG_COLOR;
    }

    if (mp_topbanner) {
        mp_topbanner.style.backgroundColor = BG_COLOR;
    }

    if (mp_left) {
        mp_left.style.backgroundColor = BG_COLOR;
    }

    if (mp_right) {
        mp_right.style.backgroundColor = BG_COLOR;
    }

    if (mp_lower) {
        mp_lower.style.backgroundColor = BG_COLOR;
    }

    for (const mp_h2 of mp_h2s) {
        mp_h2.style.backgroundColor = BG_COLOR;
    }

    for (const vector_header_container of vector_header_containers) {
        vector_header_container.style.backgroundColor = BG_COLOR;
    }

    if (vector_toc) {
        vector_toc.style.backgroundColor = BG_COLOR;

        document.addEventListener("scroll", () => {
            const vector_toc_links = document.getElementsByClassName("vector-toc-link");

            for (const link of vector_toc_links) {
                if (link.parentElement.classList.contains("vector-toc-list-item-active")) {
                    link.style.color = FG_COLOR;
                } else {
                    link.style.color = LINK_BLUE;
                }
            }
        });
    }

    for (const vector_tab_noicon of vector_tab_noicons) {
        if (vector_tab_noicon.classList.contains("selected")) {
            const spans = vector_tab_noicon.getElementsByTagName("span");

            for (const span of spans) {
                span.style.color = FG_COLOR;
            }
        }
    }

    for (const container of mw_page_containers) {
        container.style.backgroundColor = BG_COLOR;
    }

    for (const h1 of h1s) {
        h1.style.color = FG_COLOR;
    }

    for (const h2 of h2s) {
        h2.style.color = FG_COLOR;
    }

    for (const h3 of h3s) {
        h3.style.color = FG_COLOR;
    }

    for (const h4 of h4s) {
        h4.style.color = FG_COLOR;
    }

    for (const h5 of h5s) {
        h5.style.color = FG_COLOR;
    }

    for (const h6 of h6s) {
        h6.style.color = FG_COLOR;
    }

    for (const ambox of amboxen) {
        ambox.style.backgroundColor = BG_COLOR;
    }

    for (const sidebar of sidebars) {
        sidebar.style.backgroundColor = BG_COLOR;
    }

    for (const infobox of infoboxen) {
        infobox.style.color = FG_COLOR;
        infobox.style.backgroundColor = BG_COLOR;
    }

    for (const thumbinner of thumbinners) {
        thumbinner.style.backgroundColor = BG_COLOR;
    }

    for (const portalborder of portalborders) {
        portalborder.style.backgroundColor = BG_COLOR;
    }

    for (const navbox of navboxen) {
        navbox.style.backgroundColor = BG_COLOR;
    }

    for (const navbox_title of navbox_titles) {
        navbox_title.style.backgroundColor = BG_NAVBOX_BLUE;
    }

    for (const navbox_group of navbox_groups) {
        navbox_group.style.backgroundColor = BG_NAVBOX_BLUE;
    }

    for (const navbox_subgroup of navbox_subgroups) {
        navbox_subgroup.style.backgroundColor = BG_COLOR;
    }

    for (const navbox_even of navbox_evens) {
        navbox_even.style.backgroundColor = BG_TONE;
    }

    if (catlinks) {
        catlinks.style.backgroundColor = BG_COLOR;
    }

    for (const quotebox of quoteboxen) {
        quotebox.style.backgroundColor = BG_COLOR;
    }

    for (const mw_header of mw_headers) {
        mw_header.style.backgroundColor = BG_COLOR;
    }

    for (const mw_footer of mw_footers) {
        const lis = mw_footer.getElementsByTagName("li");

        for (const li of lis) {
            li.style.color = FG_COLOR;
        }
    }
})();

